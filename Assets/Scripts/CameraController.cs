﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{
    [Header("Camera settings")]
    [SerializeField] [Range(45, 89)] float Y_MAX = 85f; //константа
    [SerializeField] [Range(1, 44)] float Y_MIN = 20f;

    [SerializeField] float MouseSens = 4;
    [SerializeField] float MoveSens = 0.1f;

    [SerializeField] float distMin = 1;
    [SerializeField] float distMax = 10;

    float distance = 5.0f;
    float h = 0, v = 0, vert = 0;
    Vector3 camMov = Vector3.zero;


    //самый страшный кусок проекта, так как неоптимизирован и очень ккостылен
    // в основной он не пойдёт, так что я просто не нашёл в себе мужетво перебрать эту груду кода и 
    // сделать норм. Но во своими функиями он справляется, всё хорошо.
    [Header("Creator settings")]
    [SerializeField] GameObject PlacesInfoPanel;
    [SerializeField] GameObject PetyaInfoPanel;
    [SerializeField] GameObject DefaultButtonPreafab;
    [SerializeField] GameObject Selector;
    [SerializeField] GameObject Shop;
    [SerializeField] GameObject SpawnPointPrefab;
    [SerializeField] Terrain t;
    GameObject NowSelec;
    NewPController HandWarkPlayer;
    Dictionary<string, Color> BaseColors = new Dictionary<string, Color>
    {
        {"зелёный", new Color(0,1,0) },
        {"красный", new Color(1,0,0) },
        {"синий" , new Color(0,0,1)}

    };
    RaycastHit hit;
    int NowType = 0;
    bool BuildingMode = false;
    PointInfo NowPointInfo;
    PetyaController NowPetyaContriller;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !BuildingMode)
        {


            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit))
            {
                
                if (hit.collider.GetComponent<PointInfo>() && !PlacesInfoPanel.active)
                {
                    // меняет и вывводит данные о зданиях
                    PlacesInfoPanel.SetActive(true);
                    NowPointInfo = hit.collider.GetComponent<PointInfo>();
                    PlacesInfoPanel.transform.GetChild(0).GetComponent<Text>().text = NowPointInfo.GetMyID();
                }


                else if (hit.collider.GetComponent < PetyaController>() && hit.collider.GetComponent<PetyaController>().enabled)
                {
                    // выводит данные о людях, Buttom, потаму что так проще. плюс, задел на будующее
                    PetyaInfoPanel.SetActive(true);
                    NowPetyaContriller = hit.collider.GetComponent<PetyaController>();
                    PetyaInfoPanel.transform.GetChild(2).GetComponent<Image>().fillAmount = NowPetyaContriller.GetMyHappiness() / 100f;
                    /**
                    Dictionary<int,NavMeshPath> tmp = NowPetyaContriller.GetTimetable();
                    
                    foreach (Transform item in PetyaInfoPanel.transform.GetChild(0))
                    {
                        Destroy(item.gameObject);
                    }
                    foreach (int item in tmp.Keys)
                    {
                        GameObject g = GameObject.Instantiate(DefaultButtonPreafab, PetyaInfoPanel.transform.GetChild(0));
                        
                        g.transform.GetChild(0).GetComponent<Text>().text = "К "+item +" в " + tmp[item].gameObject.GetComponent<SpawnpointController>().MyPoint.GetMyID();
                    }
                    foreach (Transform item in PetyaInfoPanel.transform.GetChild(3))
                    {
                        Destroy(item.gameObject);
                    }
                    int[,] tm = NowPetyaContriller.GetPreferences();
                    for (int i = 0; i < tm.GetLength(0); i++)
                    {
                        GameObject g = GameObject.Instantiate(DefaultButtonPreafab, PetyaInfoPanel.transform.GetChild(3));
                        g.transform.GetChild(0).GetComponent<Text>().text ="хочет "+ Manager.GetNameFromID(tm[i, 0]) +" "+ tm[i, 1].ToString();
                        if (Manager.Instance().CheckPlace(tm[i, 0]) == null) {
                            g.GetComponent<Image>().color = BaseColors["красный"];
                       
                                }
                        else
                        {
                            g.GetComponent<Image>().color = BaseColors["зелёный"];
                        }
                    }
                    */
                }
                /**
                //else if(!hit.collider.GetComponent<NewPController>())
                {
                    if (Manager.Instance().TestPlayer != null)
                    {
                        Manager.Instance().TestPlayer.GetComponent<TestPlayerController>().RandomTP(hit.point);
                    }
                    else
                    {
                        Manager.Instance().TEST_CreateTestPalyer(hit.point);
                    }
                }
                */
                else if (hit.collider.GetComponent<NewPController>())
                {
                    hit.collider.GetComponent<NewPController>().DEBUD_showManeInfo();
                }
            }
        }

        if (BuildingMode)
        {
            
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit)&&hit.collider.gameObject.tag=="Terra")
            {
                // строит селектор на месте курсора. Вся далнейшая работа по созданию здания ложится на него
                Vector3 Target = hit.point;
                Target.x = ((int)Target.x) + 0.5f;
                
                Target.y += 0.2f;
                Target.z = ((int)Target.z) + 0.5f;
                if (NowSelec == null || NowSelec.transform.position != Target)
                {
                    Destroy(NowSelec);
                    // теперь эти 4 строчки заменили огромный код, котрый тут был. И это хорошо
                    NowSelec = GameObject.Instantiate(Selector, Target, Quaternion.identity, CityManager.GetCity().transform);
                    NowSelec.AddComponent<BuildConfig>();
                    NowSelec.GetComponent<BuildConfig>().AddManePref(Selector);//с этим надо что-то сделать, но пока  не придумал что. Хотя идея есть                    
                    NowSelec.GetComponent<BuildConfig>().GenerateBuildMap(Type: NowType);
                }             
            }

            if (Input.GetMouseButtonDown(0)&&NowSelec!=null )
            {
                //отдаёт команду строить здание. при этом время строительства 10 секунд

                NowSelec.GetComponent<BuildConfig>().StartBuild(10);
                NowSelec = null;//без этого всё не работает, так как при перемещении дестроится всё.
                
            }

            if (Input.GetMouseButtonDown(1) )
            {
                // поворачивает зданий. Работа ещё в процессе, в случае добавления генератора 
                //домов будут внесены существеные изменения
                CityManager.Instance().RotateMatrix(NowType);
                Destroy(NowSelec);
            }
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            // вход/выход из режима строительства с отрисовкой интерфейса и запеканием навигатора в конце
            BuildingMode = !BuildingMode;
            Shop.SetActive(BuildingMode);

            if (BuildingMode)
            {
                foreach (Transform item in Shop.transform)
                {
                    Destroy(item.gameObject);
                }
                string[] tmpNames = BuildConfig.Instance().GetNames();
                for (int i = 0; i < tmpNames.Length; i++)
                {
                    GameObject g = GameObject.Instantiate(DefaultButtonPreafab, Shop.transform);
                    g.name = i.ToString();
                    g.GetComponent<Button>().onClick.AddListener(delegate { ChangeType(int.Parse(g.name)); });
                    g.transform.GetChild(0).GetComponent<Text>().alignment = TextAnchor.LowerCenter;
                    g.transform.GetChild(0).GetComponent<Text>().text = tmpNames[i];

                }

            }
            else
            {
                CityManager.GetCityNavMeshSurface().UpdateNavMesh(CityManager.GetCityNavMeshSurface().navMeshData);
                Destroy(NowSelec);
            }
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            // я честно не помню зачем тут это нужно, но вроде как это для путешествий во времени. 
            // обыяно я другую букву использую, но сейчас видимо так.
            NowType += 1;
            if (NowType > 1)
            {
                NowType = 0;
            }        
        }

        #region kb mouse input
        if (Input.GetMouseButton(1) && !EventSystem.current.IsPointerOverGameObject())
        {
            h = -Input.GetAxis("Mouse X") * MouseSens;
            v = Input.GetAxis("Mouse Y") * MouseSens;
        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
            distance -= Mathf.Sign(Input.GetAxis("Mouse ScrollWheel"));
        distance = Mathf.Clamp(distance, distMin, distMax);

        if (Input.GetKey(KeyCode.LeftShift))
            vert = 1;
        else if (Input.GetKey(KeyCode.LeftControl))
            vert = -1;
        else
            vert = 0;

        camMov = new Vector3(Input.GetAxis("Horizontal"), vert, Input.GetAxis("Vertical"));
        #endregion
    }

    private void LateUpdate()
    {
        this.transform.parent.parent.Rotate(Vector3.up * this.h);
        this.transform.parent.Rotate(Vector3.right * this.v);
        this.transform.parent.rotation = Quaternion.Euler(Mathf.Clamp(this.transform.parent.rotation.eulerAngles.x, this.Y_MIN, this.Y_MAX), this.transform.parent.rotation.eulerAngles.y, 0.0f);
        this.transform.localPosition = -Vector3.forward * this.distance;

        this.transform.parent.parent.Translate(camMov * MoveSens, this.transform.parent.parent);

#warning Please set limits for cam movement
        //this.transform.parent.parent.position = new Vector3(Mathf.Clamp(this.transform.parent.parent.position.x, -200f, 200f), this.transform.parent.parent.position.y, Mathf.Clamp(this.transform.parent.parent.position.z, -200f, 200f));
    }


    public void ChangeID()
    {
       // меняет ID текущего здания на то, которое получится по названию и категории
        string text1 = PlacesInfoPanel.transform.GetChild(1).GetComponent<InputField>().text;
        NowPointInfo.СhangeID(text1);

    }
    
    public void ChangeType(int Newtype)
    {
        //метод вызывается каждый раз когда меняется выбраное здание и заменяет текущий ID
        NowType = Newtype;
        Destroy(NowSelec);
    }
   
    public void DeleteBuild()
    {
        //метод удаления здания. На данный момент работа над этой функцией в процессе
        CityManager.Instance().RemovePoint(NowPointInfo.gameObject.transform);
        foreach (GameObject item in NowPointInfo.GetAllBuild())
        {
            Destroy(item);
        }
        if (NowPointInfo.gameObject != null)
        {
            Destroy(NowPointInfo.gameObject);
        }
    }
}
