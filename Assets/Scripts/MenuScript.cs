﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScript : MonoBehaviour
{
    public void LoadMap()
    {
        GameManager.Instance().LoadMap();
    }
    public void RandomTerrain()
    {
        GameManager.Instance().RandomTerrain();
    }
    public void ExitGame()
    {
        GameManager.Instance().ExitGame();
    }
}
