﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public GameObject Text;
    public void translate(){
        if(count == 1){
            NextTutorialStep();
        }
    }
    public void updown(){
        if(count == 2){
            NextTutorialStep();
        }
    }
    public void carmove(){
        if(count == 3){
            NextTutorialStep();
        }
    }

    public void traffic(){
        if(count == 4){
            NextTutorialStep();
        }
    }
    public void speedsign(){
        if(count == 5){
            NextTutorialStep();
        }
    }
    private string[] Texts = new string[]
    {
        "Давайте попробуем освоить управление: используйте 'WASD' для перемещения ~",
        "Используйте Shift и control для подъёма и опускание камеры ~",
        "Теперь попробуем управлять машиной!) Для этого нажмите на саму машину одним кликом, а теперь попробуйте нажать на дорогу что бы машина двинулась в путь!",
        "Итак, уже хорошо. Теперь посмотрим некоторые объекты дорожного движения. Попробуйте нажать светофор, и поменяйте любой параметр)",
        "Теперь тоже самое сделайте с дорожным знаком ограничения скорости.",
        "Поздравляем, пока что вы усвоили самые основные и главные принципы игры! Попробуйте поэкспереминтировать со всем сами, и наблюдайте за городом!)"
    };

    private int count = 0;

    void NextTutorialStep()
    {
        Text.GetComponent<UnityEngine.UI.Text>().text = Texts[count];
        count += 1;
    }

    private void Start()
    {
         NextTutorialStep();
    }

}
