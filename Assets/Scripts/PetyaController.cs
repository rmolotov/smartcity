﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PetyaController : MonoBehaviour
{
    NavMeshAgent MyNavMeshAgent;
    Dictionary<int, NavMeshPath> Timetable = new Dictionary<int, NavMeshPath> { };
    int Age;
    int[,] Preferences;
    int Happiness = 100;
    public Transform Home;
    [SerializeField] GameObject NT;
    Vector3 NowTarget;
    // Start is called before the first frame update
    void Start()
    {
        //рандомный генератор персонажа. Метод, в котором он должен быть затопило 
        // во время урогана и пока что он тут на времнном месте, но новый метод готовится
        Age = UnityEngine.Random.RandomRange(0, 99);
        Preferences = new int[UnityEngine.Random.RandomRange(1, 10), 2];
        for (int i = 0; i < Preferences.GetLength(0); i++)
        {
            Preferences[i, 0] = UnityEngine.Random.RandomRange(1, 5);
            Preferences[i, 1] = UnityEngine.Random.RandomRange(1, 5);
        }
        MyNavMeshAgent = GetComponent<NavMeshAgent>();
        
        


    }

    // Update is called once per frame
    void Update()
    {
        if (NowTarget != null && Vector3.Distance(transform.position, NowTarget) <= 1)
        {
            // заход в дома. Не слишком хорошо, но работает
            // RemainDistance не работает, так как бывают глюки
            transform.localScale = new Vector3(0.04f, 0.4f, 0.04f);
            MyNavMeshAgent.isStopped = true;
            gameObject.GetComponent<MeshRenderer>().enabled = false;
        }



    }

    void GenerateTimetable(int StartTime=1)
    {
        // метод создаёт распиание используя как случайные места так и пытясь пойти туда, куда хочет
        // если желаемого мета нет, то ему плохо и он снановится чуть менее счастливым, если удаётся, 
        // то незначитьло счастливее
        int Time = StartTime;
        Transform LastPoint = transform;
        while (Time <= 20)
        {

            if (UnityEngine.Random.RandomRange(1, 100) > 60)
            {
                Transform tmp = CityManager.GetRandomPlases().transform;
                int lastTime = Time - Correct(LastPoint, tmp);
                if (lastTime <= 0)
                {
                    continue;
                }
                MyNavMeshAgent.CalculatePath(tmp.position, Timetable[lastTime]);
                Time += UnityEngine.Random.RandomRange(1, 5);
                LastPoint = tmp;
            }
            else
            {
                int Target = UnityEngine.Random.RandomRange(0, Preferences.GetLength(0) - 1);
                GameObject TargetObject = CityManager.Instance().CheckPlace(Preferences[Target, 0]);
                if (TargetObject != null)
                {

                    Transform tmp = TargetObject.transform;
                    int lastTime = Time - Correct(LastPoint, tmp);
                    if (lastTime <= 0)
                    {
                        continue;
                    }
                    MyNavMeshAgent.CalculatePath(tmp.position, Timetable[lastTime]);
                    Time += UnityEngine.Random.RandomRange(1, 5);
                    LastPoint = tmp;
                    Happiness++;
                }
                else
                    Happiness -= Preferences[Target, 1];
            }
        }
        MyNavMeshAgent.CalculatePath(Home.position, Timetable[21]);

    }

    int Correct(Transform StartPoint, Transform EndPoint)
    {
        //по изначальной щадумке должен корректировать воемя начала двидения так
        // чтобы человек не опаздывал. Но из-за нелинейной скорости пока что не работет
        NavMeshPath NMP = new NavMeshPath();
        NavMesh.CalculatePath(StartPoint.position, EndPoint.position, NavMesh.AllAreas, NMP);
        float dest = 0;
        Vector3 Last = new Vector3();
        foreach (Vector3 item in NMP.corners)
        {
            if (Last != null)
            {
                dest += Vector3.Distance(Last, item);
            }
            Last = item;
        }

        //int res = (int)(dest / NVA.speed / 10);//// не рабоет для работы всего отсльного
        //if (res == 0)
        //{
        //    res = 1;
        //}
        return 0;
    }

    public Dictionary<int, NavMeshPath> GetTimetable()
    {
        // вспомогательный метод. Нужен только для выведения расписания для отладки
        return Timetable;
    }

    public void OnChangeTime(int t)
    {
        // вызывается каждый раз когда меняется вермя в Manager

        if (t == 0)
        {
            //кадый новый день расписание перегенерируется
            Timetable.Clear();
            GenerateTimetable();

        }
        NavMeshPath NextTarget;
        if (Timetable.TryGetValue(t, out NextTarget)) ;
        {
            if (NextTarget != null)
            {
                MyNavMeshAgent.isStopped = false;
                transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                gameObject.GetComponent<MeshRenderer>().enabled = true;
                MyNavMeshAgent.SetPath(NextTarget);
                
                Timetable.Remove(t);
            }
        }

    }

    public int GetMyHappiness()
    {
        // выводит уровень счастья
        return Happiness;
    }

    //public void CheckRemovePlase(Transform tmp)
    //{
    //    if (Timetable.ContainsValue(tmp))
    //    {
    //        List<int> Rem = new List<int>(0);
    //        foreach (int item in Timetable.Keys)
    //        {
                
                
    //                if (Timetable[item].position == tmp.position)
    //                {
    //                Rem.Add(item);

    //                }
                
                
    //        }
    //        foreach (int item in Rem)
    //        {
    //            Timetable.Remove(item);
    //        }
            
    //    }
    //    if(MyNavMeshAgent.destination == tmp.position)
    //    {
    //        if (Home != null)
    //        {
    //            MyNavMeshAgent.SetDestination(Home.position);
    //        }
    //        else
    //        {
    //            MyNavMeshAgent.isStopped = true;
    //            transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
    //            gameObject.GetComponent<MeshRenderer>().enabled = true;
    //        }
    //    }
    //    if (Home == tmp)
    //    {
    //        transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
    //        gameObject.GetComponent<MeshRenderer>().enabled = true;
    //    }
    //}

    public int[,] GetPreferences()
    {
    //выводит список желаемых мест. Нужен исключительно для отладки
        return Preferences;
    }
    
}
