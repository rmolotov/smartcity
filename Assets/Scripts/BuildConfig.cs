﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System;
using UnityEngine.UI;

public class BuildConfig : MonoBehaviour
{
    [Header("Building Presets")]
    string[] BuildingNames = new string[] { "дорога", "маленький дом", "дом культуры", "большой дом", "школа", "стадион" };
    int[][,] BuildingConfigurations = new int[][,]
    {
        new int[,] {
            { 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1 },
            { 1, 1, 2, 1, 1 },
            { 1, 1, 1, 1, 1 },
            { 1, 1, 1, 1, 1 }
        },
        new int[,] {
            { 1, 2, 2, 1, 1 },
            { 1, 2, 2, 1, 1 },
            { 4, 3, 2, 1, 1 },
            { 1, 2, 2, 1, 1 },
            { 1, 1, 1, 1, 1 }
        },
        new int[,] {
            { 2, 2, 1, 1, 1 },
            { 2, 2, 2, 3, 4 },
            { 2, 2, 2, 2, 1 },
            { 2, 2, 2, 2, 1 },
            { 2, 2, 1, 1, 1 }
        },
        new int[,] {
            { 1, 1, 4, 1, 1 },
            { 2, 2, 3, 2, 2 },
            { 2, 2, 2, 2, 2 },
            { 2, 2, 2, 2, 2 },
            { 1, 1, 1, 1, 1 }
        },
        new int[,] {
            { 2, 1, 4, 1, 2 },
            { 2, 2, 3, 2, 2 },
            { 2, 2, 2, 2, 2 },
            { 2, 1, 1, 1, 2 },
            { 2, 1, 1, 1, 2 }
        },
        new int[,] {
            { 1, 2, 2, 2, 1 },
            { 2, 1, 1, 1, 2 },
            { 2, 1, 1, 1, 2 },
            { 1, 2, 3, 2, 1 },
            { 1, 1, 4, 1, 1 }
        }
    };

    List<GameObject> MyBuildMap=new List<GameObject>(0);
    int MyType = -1;
    bool CanBuild = true;
    GameObject MePrefab;
    [SerializeField] GameObject[] Prefabs;
    bool LetsBuild = false;
    float Timer = 0;

    #region Singleton
    private static BuildConfig _instance;
    public static BuildConfig Instance() { return _instance; }
    
    
    void Awake()

    {
        if (!_instance)
        {
            _instance = this;
        }

    }
    #endregion Singleton


    private void Update()
    {
        if (LetsBuild)
        {
            Timer -= Time.deltaTime;
            if (Timer < 0)
            {
                Build();
            }
        }

       
    }





    public int[][,] GetConfig()
    {
       // возвращает конфигурацию, нужен только для менеджера, так как переносить всё туда не рационально.

        return BuildingConfigurations;
    }

    public string[] GetNames()
    {
        // что то мне подсказывает, что это нужно при отрисовке целей. Но может и нет. Как минимум не мешает.
        return BuildingNames;
    }

    public void AddType(int type) {
        // метод для добавления типа. Не объединён со следующим потому, что они иногда нужны по отдельности.

        MyType = type;
        
    }

    public void AddManePref(GameObject Pref)
    {
        // Аналогично предыдущему. Только тут префаб. Работа ведётся над более нормальной системой
        MePrefab = Pref;
    }
    

    public void GenerateBuildMap(int Type = -1)
    {
        // метод создаёт сетку выделения буж=дующего здания. входныйе данный расчитаны на 2 различных вида использования
        // в генераторе и при прямом строительстве
        if (Type != -1)
        {
            MyType = Type;
        }
        
        int[,] NowBuild = CityManager.GetNowBuildingConfiguration(MyType);

        if(!CityManager.Instance().CheckFreeSpace((int)transform.position.x, (int)transform.position.z))
        {
            gameObject.GetComponent<MeshRenderer>().material.color = new Color(1, 0, 0);
        }
        for (int i = 0; i < NowBuild.GetLength(0); i++)
        {
            for (int j = 0; j <NowBuild.GetLength(1); j++)
            {
                if (NowBuild[i, j] != 1)
                {
                    Vector3 NewTar = transform.position;
                    NewTar.x -= 2 - i;
                    NewTar.z -= 2 - j;
                    GameObject gq = GameObject.Instantiate(MePrefab, NewTar, Quaternion.identity, transform);
                    if(NowBuild[i, j] == 4)
                    {
                        gq.GetComponent<MeshRenderer>().material.color = new Color(0, 1, 0);
                        gq.tag = "SPoint";
                        // место входа  в здание подсвечено отдельно. Там ничего не строится, но эту клетку нельзя занемать
                    }
                    if (NowBuild[i, j] == 3)
                    {
                        gq.tag = "IPoint";
                        // главная часть здания, в последствии именно там будет мозг
                    }

                    if (!CityManager.Instance().CheckFreeSpace((int)(NewTar.x), (int)(NewTar.z)))
                    {
                        gq.GetComponent<MeshRenderer>().material.color = new Color(1, 0, 0);
                        CanBuild = false;
                        // если здание заползает на другие постройки то подсветка об этом сообщает. Плюс если есть хоть один такой блок
                        // то домик не построишь
                    }
                    MyBuildMap.Add(gq);
                }
            }
        }
    }


    private void OnDestroy()
    {
        // перед смертью убивает всех остальных
        foreach (GameObject item in MyBuildMap)
        {
            Destroy(item);
        }
    }

    public void StartBuild(int BuildTime = 10)
    {
       // метод нужен для отложеного строительства. 
       // после его запуска дом будет построен через BuildTime секунд

        if (MyType != 0)
        {
            Timer = BuildTime;
            LetsBuild = true;
            foreach (GameObject item in MyBuildMap)
            {
                item.GetComponent<MeshRenderer>().material.color = new Color(0, 0, 1);
            }
            gameObject.GetComponent<MeshRenderer>().material.color = new Color(0, 0, 1);


        }
        else
        {
            // это для дороги, она строится мгновенно
            Build();
        }
        
    }



    public void Build()
    {
        // основной метод строительства. Ничем принципиально не отличается от старого, просто проверять пересечения не надо
        // всё уже проверено, так что , по идеи, это должно экономить ресурсы компьютера
        GameObject Pref = CityManager.Instance().Builds[MyType];
        if (CanBuild)
        {
            List<GameObject> tmp = new List<GameObject>(0);
            GameObject g = null;
            GameObject Mane = null;
            GameObject SP = null;
            foreach (GameObject item in MyBuildMap)
            {
                if (item.tag == "SPoint")
                {
                    g = GameObject.Instantiate(CityManager.Instance().SpawnPoint, item.transform.position, Quaternion.identity, transform.parent);
                    SP = g;

                    //создание точни спавна. на деле это просто пустышка, но она нужна
                    
                }
                else
                {
                    g = GameObject.Instantiate(Pref, item.transform.position, Quaternion.identity, transform.parent);
                    g.GetComponent<MeshFilter>().mesh.name = GenerateRandomName();
                }
                if(item.tag == "IPoint")
                {
                    g.AddComponent<PointInfo>();

                    // добавление скрипта управления. 
                    Mane = g;
                    
                }
                tmp.Add(g);
                CityManager.Instance().AddNewBuild(g);
                

            }
            if (Mane != null)
            {
                Mane.GetComponent<PointInfo>().InstalBuild(tmp.ToArray(), MyType, SP);
                SP.GetComponent<SpawnpointController>().MyPoint = Mane.GetComponent<PointInfo>();
                SP.GetComponent<SpawnpointController>().Check();
                CityManager.Instance().AddNewPoint(Mane);
                Mane.tag = "IPoint";
                //добавление в управляющий скрипт всего что ему нужно
            }
            if (LetsBuild)
            {
                CityManager.GetCityNavMeshSurface().UpdateNavMesh(CityManager.GetCityNavMeshSurface().navMeshData);
                // однавление навигационного меша, это неизбежно, ведь мы не строим мгновенно

            }


        }
        Destroy(gameObject);
    }

    string GenerateRandomName()
    {
        bool digitOrLetter = false;
        string randomWord = "";
        
        //A-Z (65-90) a-z(97-122) 0-9(48-57)
        for (int i = 0; i < 10; i++)
        {
            if (digitOrLetter)
            {
                randomWord += (char)UnityEngine.Random.RandomRange(65, 90);
            }
            else
            {
                randomWord += (char)UnityEngine.Random.RandomRange(48, 57);
            }
            digitOrLetter = !digitOrLetter;
        }
        return randomWord;
    }
    

}

    
