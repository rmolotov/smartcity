﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SpeedSignController : MonoBehaviour
{
    public int MaxSpeed = 120;
    public speedcontrolller trigger;
    [SerializeField]GameObject Info;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RegSign(int MS)
    {
        MaxSpeed = MS;
        Info.GetComponent<TextMeshProUGUI>().text = MaxSpeed.ToString();
    }
}
