﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CarController : MonoBehaviour
{
    public NavMeshAgent MyNavMeshAgent;
    NavMeshObstacle obstacle;
    List<GameObject> Points = new List<GameObject>();
    bool OnLink = false;
    bool stopped = false;
    public float DefaultSpeed = 3;
    public float OnLinkSpeed = 0.5f;
    bool need_draw = false;
    float NowMaxSpeed;
    GameObject pf;
    
    void Start()
    {
        MyNavMeshAgent = GetComponent<NavMeshAgent>();
        obstacle = this.GetComponent<NavMeshObstacle>();
        DefaultSpeed = UnityEngine.Random.Range(1f, 3f);
        NowMaxSpeed = DefaultSpeed;
        
    }

    CarController get_intersection(CarController v){
        if(v.MyNavMeshAgent.path.corners.Length > 1){
            Vector3 direction = v.MyNavMeshAgent.path.corners[1] - v.transform.position;
            RaycastHit hit;
            //Debug.DrawRay(v.transform.position, direction / direction.magnitude , Color.green);
            if(Physics.Raycast(v.transform.position, direction, out hit, 1f)){
                if(hit.collider && hit.collider.GetComponentInParent<CarController>()){
                    return hit.collider.GetComponentInParent<CarController>();
                }
            }
        }
        return null;
    }

    void Update(){
        if (need_draw)
        {
            draw();
        }
        if (stopped)
        {
            this.MyNavMeshAgent.isStopped = true;
            return;
        }
        CarController bf = get_intersection(this);
        if(bf != null){
            CarController other = get_intersection(bf);
            if(other != this){
                this.MyNavMeshAgent.isStopped = true;

            }else{
                this.MyNavMeshAgent.isStopped = false;
            }
        }else{
            this.MyNavMeshAgent.isStopped = false;
        }
        /*
        if(MyNavMeshAgent.remainingDistance < 0.5f) {
            obstacle.enabled = false;
        }else{
            obstacle.enabled = false;
        }*/
        if (MyNavMeshAgent.isOnOffMeshLink)
        {   if(!OnLink){
                OnLink = !OnLink;
                if(OnLink){
                    MyNavMeshAgent.speed = Mathf.Min(DefaultSpeed, NowMaxSpeed) * 0.35f;
                }else{
                    MyNavMeshAgent.speed = Mathf.Min(DefaultSpeed, NowMaxSpeed);
                }
            }
        
        }else{
            if(OnLink){
                OnLink = !OnLink;
                if(OnLink){
                    MyNavMeshAgent.speed = Mathf.Min(DefaultSpeed, NowMaxSpeed) * 0.35f;
                }else{
                    MyNavMeshAgent.speed = Mathf.Min(DefaultSpeed, NowMaxSpeed);
                }
            }
        }
    }

    public void NewTarget(Vector3 Targ, GameObject Pref)
    {

        MyNavMeshAgent.SetDestination(Targ);
        NavMeshPath MyNavNeshPath = new NavMeshPath();
        MyNavMeshAgent.CalculatePath(Targ, MyNavNeshPath);
        pf = Pref;
        need_draw = true;
    }
    public void draw() {
        if (!MyNavMeshAgent.pathPending)
        {
            return;
        }
        need_draw = false;
        ClearPathLine();
        Vector3 LastI = transform.position;
        //print(MyNavNeshPath.corners.Length);
        foreach (Vector3 item in this.MyNavMeshAgent.path.corners)
        {
            GameObject g = GameObject.Instantiate(pf, transform.parent);
            
            LineRenderer LR = g.GetComponent<LineRenderer>();
            List<Vector3> pos = new List<Vector3>();
            pos.Add(item);
            pos.Add(LastI);
            LastI = item;
            LR.startWidth = 0.2f;
            LR.endWidth = 0.2f;
            LR.SetPositions(pos.ToArray());
            Points.Add(g);
        }

    }

    public void ClearPathLine()
    {
        foreach (GameObject item in Points)
        {
            Destroy(item);
        }
        Points.Clear();
        
    }

    public void TrafficLightStatus(int conc)
    {
        if(conc == 2)
        {
            MyNavMeshAgent.isStopped = true;
            stopped = true;
        }
        else if(conc == 1)
        {
            MyNavMeshAgent.speed = Mathf.Min(DefaultSpeed, NowMaxSpeed)/2f;
            stopped = false;
        }
        else if(conc == 3)
        {
            MyNavMeshAgent.speed = Mathf.Min(DefaultSpeed, NowMaxSpeed);
            MyNavMeshAgent.isStopped = false;
            stopped = false;

        }
        else if(conc == 0)
        {
            MyNavMeshAgent.speed = Mathf.Min(DefaultSpeed, NowMaxSpeed);
            MyNavMeshAgent.isStopped = false;
            stopped = false;
        }
    }


    public void SpeedControll(int Ogran)
    {
        // возьмем 1 скорость = 36 км\ч
        float NewSpeedOgran = Mathf.Min(DefaultSpeed, (float)(Ogran) / 36f);
        
        NowMaxSpeed = NewSpeedOgran;
        MyNavMeshAgent.speed = Mathf.Min(DefaultSpeed, NowMaxSpeed);

       
    }
    public void NoOgran()
    {
        NowMaxSpeed = DefaultSpeed;
        MyNavMeshAgent.speed = DefaultSpeed;stopped = false;
        MyNavMeshAgent.isStopped = false;
    }
    
}

