﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] Canvas HUD;

    #region Singleton
    private static GameManager _instance;
    public static GameManager Instance() { return _instance; }

    private void Awake()
    {
        if (!_instance)
        {
            _instance = this;
            DontDestroyOnLoad(this);
            DontDestroyOnLoad(HUD);
        }
        else Destroy(this);
        SceneManager.LoadScene(1);
    }
    #endregion Singleton

    private void OnLevelWasLoaded(int level)
    {
        HUD.gameObject.SetActive(level > 1);
    }

    public void LoadMap()
    {
        //todo
    }
    public void RandomTerrain()
    {
        SceneManager.LoadScene(2);
    }
    public void ReturnToMenu()
    {
        SceneManager.LoadScene(1);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
