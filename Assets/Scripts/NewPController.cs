﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NewPController : MonoBehaviour
{
    #region Характеристики
    int Age;
    int Money;
    int Happy=100;
    int[,] Preferences;
    Transform Home;

    #endregion Характеристики

    [SerializeField]Transform NowTarget;
    Transform LastTarget;
    int NextTargetTime = -1;
    int NowTime = 0;
    bool Change = false;
    NavMeshAgent MyNavMeshAgent;
    void Start()
    {
        MyNavMeshAgent = GetComponent<NavMeshAgent>();
        InitPeapleData();
    }

    // Update is called once per frame
    void Update()
    {
        if (Change)
        {
         // позволяет ускорить работу при выборе новой цели   
            if (NowTime == NextTargetTime)
            {
                NewTarget();
                NextTargetTime += UnityEngine.Random.RandomRange(1, 4);
                if (NextTargetTime > 23)
                {
                    NextTargetTime -= 23;
                }
            }
            Change = false;
        }

        #region Вход в здание
        if (!MyNavMeshAgent.isStopped && NowTarget != null && MyNavMeshAgent.remainingDistance<1.5f)
        {
            // заход в дома. Не слишком хорошо, но работает
            // попытка сделать всё хорошо. На данный момент это лучшее решение
            transform.localScale = new Vector3(0.04f, 0.4f, 0.04f);
            MyNavMeshAgent.isStopped = true;
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            
        }
        if (MyNavMeshAgent.remainingDistance > 1.5f)
        {
            // необходим для ликвидации бага с исчезновением
            //людей при перепекании межа.
            MyNavMeshAgent.isStopped = false;
            gameObject.GetComponent<MeshRenderer>().enabled = true;
            transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
        }

        if (!MyNavMeshAgent.isStopped &&LastTarget != null && Vector3.Distance(transform.position, LastTarget.position) > 1)
        {
            // необходимо для ликвидации бага с появлением жителей которые никуда не идут
            // баг странный, но он просто есть. Вероятно это связвно с тем, что люи тупят или
            //не могут найти путь. А может мой комп слишком фигня и проца не хватает. 
            // В общем это надо, хоть и плохо
            gameObject.GetComponent<MeshRenderer>().enabled = true;
        }
        #endregion Вход в здание
    }

    public void OnChangeTime(int t)
    {
        // метод вызывается менеджером при каждом изменении времени
        NowTime = t;
        Change = true;
    
    }

    void NewTarget()
    {
        // персонаж выбирает куда ему идти
        
            if (UnityEngine.Random.RandomRange(1, 100) > 60)
            {
                Transform tmp = CityManager.GetRandomPlases().transform;
                SetNewTarget(tmp);
                
            }
            else
            {

                int Target = UnityEngine.Random.RandomRange(0, Preferences.GetLength(0) - 1);
                GameObject TargetObject = CityManager.Instance().CheckPlace(Preferences[Target, 0]);
                if (TargetObject != null)
                {
                    SetNewTarget(TargetObject.transform);
                    
                }
                
            }

        
    }

    void SetNewTarget(Transform Target)
    {
        //установка новой цели
        MyNavMeshAgent.isStopped = false;
        transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
        MyNavMeshAgent.SetDestination(Target.position);
        NowTarget = Target;
    }

    float CheckDistance(Vector3 v3)
    {
        // метод возвращает растояние до цели
        NavMeshPath NMP = new NavMeshPath();
        NavMesh.CalculatePath(transform.position, v3, NavMesh.AllAreas, NMP);
        float dest = 0;
        Vector3 Last = new Vector3();
        foreach (Vector3 item in NMP.corners)
        {
            if (Last != null)
            {
                dest += Vector3.Distance(Last, item);
            }
            Last = item;
            
        }
        print(dest);
        return dest;
    }

    public void AddHome(Transform T)
    {
        //добавить дом и инициализировать юнита
        Home = T;
        LastTarget = Home;
        transform.localScale = new Vector3(0.04f, 0.4f, 0.04f);
       
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        NextTargetTime += UnityEngine.Random.RandomRange(2, 4);
    }

    void InitPeapleData()
    {
        //добавляет стандартные характеристики человеку
        //такие как возраст, бюджет, и предпочтения 
        Age = UnityEngine.Random.RandomRange(0, 99);
        Money = UnityEngine.Random.RandomRange(100, 100000);

        Preferences = new int[UnityEngine.Random.RandomRange(1, 10), 2];
        for (int i = 0; i < Preferences.GetLength(0); i++)
        {
            Preferences[i, 0] = UnityEngine.Random.RandomRange(1, CityManager.GetMaxID());
            Preferences[i, 1] = UnityEngine.Random.RandomRange(1, 5);
        }
    }



    #region DEBUG

    public void DEBUG_GoTo(Vector3 v3)
    {
        //метод общего подхода к точке
        //Работает вроде норм
        transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
        gameObject.GetComponent<MeshRenderer>().enabled = true;
        //CheckDistance(v3);
        print(v3);
        NavMeshPath path = new NavMeshPath();
        MyNavMeshAgent.CalculatePath(v3, path);

        MyNavMeshAgent.SetPath(path);
        NextTargetTime += UnityEngine.Random.RandomRange(1, 4);
        //MyNavMeshAgent.SetDestination(v3);

    }
    public void DEBUD_showManeInfo()
    {
        //выводит даные о человеке.Необходимо было
        //для настройки и отладки
        print("---------------------");
        print("Stop? -" + MyNavMeshAgent.isStopped);
        print("a = " + MyNavMeshAgent.velocity);
        print("Rem Dist =" + MyNavMeshAgent.remainingDistance);
        print("is path Stale" + MyNavMeshAgent.isPathStale);
        print("Dist(now,last)" + Vector3.Distance(transform.position, LastTarget.position));
        print("----------------------");

    }
    #endregion DEBUG
}
